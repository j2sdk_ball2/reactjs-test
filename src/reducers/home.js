import Immutable, { Record } from 'immutable';
import { ADD_DATA } from '../actions/home';

const State = Record({
  id: '',
  name: '',
  dataList: [],
});

const initialState = new State();

const home = (state = initialState, action) => {
  let _state;
  switch (action.type) {
    case ADD_DATA:
      _state = Immutable.fromJS({
        dataList: [
          ...state.dataList,
          {
            id: action.id,
            name: action.name,
          },
        ],
      });
      return state.merge(_state);
    default:
      return state;
  }
};

export default home;
