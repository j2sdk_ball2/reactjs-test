import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import store from './store';

export const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

if (!module.hot) {
  render(<Root />, document.querySelector('react'));
}
