import React from 'react';
// import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { List as ImmList } from 'immutable';

const List = ({ dataList }) => {
  if (dataList instanceof ImmList) {
    return (
      <div>
        {dataList.map(data => {
          const id = data.get('id');
          const name = data.get('name');
          return <div key={id}>{id} : {name}</div>;
        })}
      </div >
    );
  }
  return <span>No data</span>;
};

List.propTypes = {
  dataList: PropTypes.any,
};

List.defaultProps = {
  dataList: [],
};

export default List;
