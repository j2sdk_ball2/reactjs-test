import React, { Component } from 'react';
// import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import { add } from '../actions/home';

class Home extends Component {
  static propTypes = {
    onClick: PropTypes.func,
  };

  static defaultProps = {
    onClick: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
    };
  }

  // componentDidMount() {
  // }

  // componentWillReceiveProps() {
  // }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
    });
  }

  handleClick() {
    const { id, name } = this.state;
    const { onClick } = this.props;
    onClick(id, name);
    this.setState({
      id: '',
      name: '',
    });
  }

  render() {
    const { id, name } = this.state;
    return (
      <div>
        <div>
          id:<input name="id" type="text" value={id} onChange={e => this.handleChange(e)} />
        </div>
        <div>
          name:<input name="name" type="text" value={name} onChange={e => this.handleChange(e)} />
        </div>
        <div>
          <button
            onClick={e => this.handleClick(e)}
            disabled={!id || !name}
          >Add</button>
        </div>
      </div>
    );
  }
}

export default Home;
