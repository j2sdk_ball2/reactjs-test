import React from 'react';
import Home from '../containers/Home';
import List from '../containers/List';

const App = () => (
  <div>
    <Home />
    <List />
  </div>
);

export default App;
