import { connect } from 'react-redux';
import List from '../components/List';

const mapStateToProps = state => ({
  dataList: state.home.get('dataList'),
});

export default connect(
  mapStateToProps,
)(List);
