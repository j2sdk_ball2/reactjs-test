import { connect } from 'react-redux';
import { add } from '../actions/home';
import Home from '../components/Home';

const mapDispatchToProps = dispatch => ({
  onClick: (id, name) => dispatch(add(id, name)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Home);
