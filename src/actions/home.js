export const ADD_DATA = 'home/ADD_DATA';

export const add = (id, name) => ({
  type: ADD_DATA,
  id,
  name,
});
